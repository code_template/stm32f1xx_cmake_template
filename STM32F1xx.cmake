INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_PROCESSOR cortex-m3)

FIND_PROGRAM(ARM_CC arm-none-eabi-gcc ${TOOLCHAIN_DIR}/bin)
FIND_PROGRAM(ARM_CXX arm-none-eabi-g++ ${TOOLCHAIN_DIR}/bin)
FIND_PROGRAM(ARM_OBJCOPY arm-none-eabi-objcopy ${TOOLCHAIN_DIR}/bin)
FIND_PROGRAM(ARM_SIZE_TOOL arm-none-eabi-size ${TOOLCHAIN_DIR}/bin)

CMAKE_FORCE_C_COMPILER(${ARM_CC} GNU)
CMAKE_FORCE_CXX_COMPILER(${ARM_CXX} GNU)

SET(CMAKE_ARM_FLAGS
        "-mcpu=cortex-m3 -mthumb -fno-common -ffunction-sections -fdata-sections"
        )

IF(CMAKE_SYSTEM_PROCESSOR STREQUAL "cortex-m3")
    SET(CMAKE_C_FLAGS"${CMAKE_C_FLAGS} ${CMAKE_ARM_FLAGS}")
    SET(CMAKE_CXX_FLAGS"${CMAKE_CXX_FLAGS} ${CMAKE_ARM_FLAGS}")
    SET(CMAKE_EXE_LINKER_FLAGS"${CMAKE_EXE_LINKER_FLAGS} ${CMAKE_ARM_FLAGS}")
ELSE()
    MESSAGE(WARNING
            "Processor not recognised in toolchain file,"
            "compiler flags not configured."
            )
ENDIF()

STRING(REGEX REPLACE ";" " " CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "")
SET(BUILD_SHARED_LIBS OFF)
