
CMAKE_MINIMUM_REQUIRED(VERSION 3.7)

PROJECT(stm32f1xx_cmake_template)

ADD_DEFINITIONS(-DSTM32F1xx.cmake)

ENABLE_LANGUAGE(ASM)

SET(BUILD_MODE DEBUG)

SET(USER_C_FLAGS "-mcpu=cortex-m3 -mthumb -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding \
        -fno-move-loop-invariants -Wall")
SET(USER_CXX_FLAGS  "-std=c++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics")
#-DUSE_FULL_ASSERT \
SET(USER_C_DEFINES
        "-DARM_MATH_CM3 \
        -DSTM32F10X_HD\
        -DTRACE \
        -DOS_USE_TRACE_SEMIHOSTING_STDOUT \
        -DSTM32F10X_HD \
        -DUSE_STDPERIPH_DRIVER \
        -DHSE_VALUE=8000000"
        )

if(BUILD_MODE STREQUAL "DEBUG")
    SET(USER_C_FLAGS "${USER_C_FLAGS} -Og -g3")
    SET(USER_C_DEFINES "${USER_C_DEFINES} -DDEBUG")
else()
    SET(USER_C_FLAGS "${USER_C_FLAGS} -O3")
endif()

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${USER_C_FLAGS} ${USER_C_DEFINES}")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${USER_C_FLAGS} ${USER_CXX_FLAGS} ${USER_C_DEFINES}")

INCLUDE_DIRECTORIES(
        # ARM-NONE EABI 编译器
        ${TOOLCHAIN_DIR}/arm-none-eabi/include
        # 芯片库文件
        ./lib/cmsis/cm3/core_support
        ./lib/cmsis/cm3/device_support/st/stm32f10x
        ./lib/fwlib/stm32f10x/inc
        # 用户文件
        ./src/app
        ./src/bsp
        ./src/ctrl
        ./src/mab
        ./src/mat
        ./src
        .
)
FILE(GLOB_RECURSE SOURCE_FILES
        # 芯片库文件
        ./lib/fwlib/stm32f10x/src/*.c
        ./lib/cmsis/cm3/startup/startup_stm32f103xb.s
        ./lib/cmsis/cm3/core_support/core_cm3.c
        ./lib/cmsis/cm3/device_support/st/stm32f10x/system_stm32f10x.c
        # 用户文件
        ./*.c
        ./src/*.c
        .
        )
SET(LIB_FILES ${CMAKE_SOURCE_DIR}/lib/fwlib/stm32f10x/lib/stm32f10x.lib)

SET(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/lib/cmsis/cm3/startup/STM32F103C8Tx_FLASH.ld)


SET(CMAKE_EXE_LINKER_FLAGS
"${CMAKE_EXE_LINKER_FLAGS} -T ${LINKER_SCRIPT} -fmessage-length=0 -fsigned-char -ffreestanding \
  -specs=nano.specs  -specs=nosys.specs -nostartfiles  -Xlinker --gc-sections -Wl,-Map=${PROJECT_NAME}.map")


ADD_EXECUTABLE(${PROJECT_NAME}.elf ${SOURCE_FILES})
#TARGET_LINK_LIBRARIES(${PROJECT_NAME}.elf ${LIB_FILES})

#SET(HEX_FILE ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.hex)
#SET(BIN_FILE ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.bin)
#
#ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME}.elf POST_BUILD
#        COMMAND ${ARM_OBJCOPY} -Oihex$<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
#        COMMAND ${ARM_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
#        )

set(HEX_FILE ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.bin)
add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
        COMMAND ${ARM_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
        COMMAND ${ARM_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
        )